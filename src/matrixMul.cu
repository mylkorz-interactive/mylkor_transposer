// System includes
#include <assert.h>
#include <stdio.h>

// CUDA runtime
#include <cuda_profiler_api.h>
#include <cuda_runtime.h>

// Helper functions and utilities to work with CUDA
#include <helper_cuda.h>
#include <helper_functions.h>

#include <cstdint>

__global__ void please_dear_god_save_me(int *input_mat, int *output_mat, int w, int h)
{
    int blockId = (gridDim.x * blockIdx.y) + blockIdx.x;

    int threadId = (blockId * (blockDim.x * blockDim.y)) + (threadIdx.y * blockDim.x) + threadIdx.x;

    int i = threadId;

    int x = i / h;
    int y = (h - 1) - (i % h);

    output_mat[i] = input_mat[y * w + x];
}

int swap_those_stds()
{
    uint32_t w = 3840;
    uint32_t h = 2160;
    uint32_t element_count = w * h;
    uint32_t buff_size = element_count * sizeof(int);

    int *host_input_buffer;
    int *host_result_buffer;

    int *expected_matrix = static_cast<int *>(malloc(buff_size));

    int *device_input_buffer;
    int *device_result_buffer;

    checkCudaErrors(cudaMallocHost(&host_input_buffer, buff_size));
    checkCudaErrors(cudaMallocHost(&host_result_buffer, buff_size));

    for (uint32_t i = 0; i < element_count; i++)
    {
        host_input_buffer[i] = i + 1;
        host_result_buffer[i] = 0.0f;
    }

    for (uint32_t i = 0; i < element_count; i++)
    {
        int x = i / h;
        int y = (h - 1) - (i % h);

        expected_matrix[i] = host_input_buffer[y * w + x];
    }

    checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&device_input_buffer), buff_size));
    checkCudaErrors(cudaMalloc(reinterpret_cast<void **>(&device_result_buffer), buff_size));

    cudaStream_t stream;
    checkCudaErrors(cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking));

    checkCudaErrors(cudaMemcpyAsync(device_input_buffer, host_input_buffer, buff_size, cudaMemcpyHostToDevice, stream));
    checkCudaErrors(
        cudaMemcpyAsync(device_result_buffer, host_result_buffer, buff_size, cudaMemcpyHostToDevice, stream));

    dim3 threads(16, 16);
    dim3 grid(w / threads.x, h / threads.y);

    printf("BLOCK COUNT(%d, %d)\nTHREADS PER BLOCK(%d, %d) -> %d\n", (int)grid.x, (int)grid.y, (int)threads.x,
           (int)threads.y, threads.x * threads.y);

    please_dear_god_save_me<<<grid, threads>>>(device_input_buffer, device_result_buffer, w, h);

    cudaEvent_t start_event, stop_event;
    checkCudaErrors(cudaEventCreate(&start_event));
    checkCudaErrors(cudaEventCreate(&stop_event));

    checkCudaErrors(cudaStreamSynchronize(stream));

    // Record the start event
    checkCudaErrors(cudaEventRecord(start_event, stream));

    // KERNEL GO HERE YES VERY FAST
    for (int i = 0; i < 1000; i++)
    {
        please_dear_god_save_me<<<grid, threads>>>(device_input_buffer, device_result_buffer, w, h);

        checkCudaErrors(
            cudaMemcpyAsync(host_result_buffer, device_result_buffer, buff_size, cudaMemcpyDeviceToHost, stream));

        checkCudaErrors(cudaStreamSynchronize(stream));
    }
    // Record the stop event
    checkCudaErrors(cudaEventRecord(stop_event, stream));
    // Wait for the stop event to complete
    checkCudaErrors(cudaEventSynchronize(stop_event));

    // Copy result from device to host
    checkCudaErrors(
        cudaMemcpyAsync(host_result_buffer, device_result_buffer, buff_size, cudaMemcpyDeviceToHost, stream));

    checkCudaErrors(cudaStreamSynchronize(stream));

    float msecTotal = 0.0f;
    checkCudaErrors(cudaEventElapsedTime(&msecTotal, start_event, stop_event));

    // Compute and print the performance
    float msecPerMatrixMul = msecTotal;
    printf("Time= %.3f msec\n", msecPerMatrixMul / 1000);

    uint32_t correct_count = 0;
    for (int i = 0; i < element_count; i++)
    {
        if (host_result_buffer[i] == expected_matrix[i])
        {
            correct_count++;
        }
    }

    printf("Correct: %d/%d (%f%%)", correct_count, element_count, ((float)element_count / (float)correct_count) * 100);

    return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
    printf("start very good yes");
    // This will pick the best possible CUDA capable device, otherwise
    // override the device ID based on input provided at the command line
    int dev = findCudaDevice(argc, (const char **)argv);

    // 3840 x 2160
    checkCudaErrors(cudaProfilerStart());
    int matrix_result = swap_those_stds();
    checkCudaErrors(cudaProfilerStop());

    exit(matrix_result);
}
